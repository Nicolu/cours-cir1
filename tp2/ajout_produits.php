<html lang="fr">
  <head>
    <meta charset="utf-8">
    <title>Ajout produit</title>
  </head>

  <body>
    <?php if(!empty($_POST['nom']) && !empty($_POST['prix']) && !empty($_POST['produits']) && !empty($_FILES['photo'])){
      echo '<p>Votre produit a bien été enregistré.</p>';
      define('TARGET_DIRECTORY', './');
      move_uploaded_file($_FILES['photo']['tmp_name'], TARGET_DIRECTORY . $_FILES['photo']['name']);
      $monfichier = fopen('mesproduits.csv','a+');
      fputs($monfichier, $_POST['nom']. ';' . $_POST['prix']. ';'.$_POST['produits']. ';'.$_FILES['photo']['name'].';');
      fclose($monfichier);
    }
    else{ ?>
    <form method="post" action="ajout_produits.php" enctype="multipart/form-data">
      <fieldset>
       <legend>Ajoutez un produit</legend> <!-- Titre du fieldset -->

       <label for="nom">Quel est le nom du produit ?</label>
       <input type="text" name="nom" id="nom" />

       <label for="prix">Quel est le prix du produit ?</label>
       <input type="text" name="prix" id="prix" />

       <label for="produits">Combien y a t-il de produits ?</label>
       <input type="texte" name="produits" id="produits" />

       <input type="file" name="photo">
     </fieldset>
     <button>Envoyer</button>
    </form>
    <?php } ?>
  </body>
</html>
