<?php
session_start();
$taille = 7;
$tab = array();
for($i=0; $i < $taille; $i++){
    $tab[$i] = array();
}
$tab2 = array();
for($i=0; $i < $taille; $i++){
    $tab2[$i] = array();
}

echo '<style> td{border : 2px solid black; width: 15px; height: 30px;text-align : center;}</style>';

function initTab($taille,$tab){
    for($i=0; $i < $taille; $i++){
        for($j=0; $j < $taille; $j++){
            if(rand(0, 1)==1){
                $tab[$i][$j] = 'O';
            }
            else{
                $tab[$i][$j] = ' ';
            }
        }
    }
    return $tab;
}
if(!isset($_SESSION['genSuivante'])){
    $tab = initTab($taille, $tab);
}
else{
    $tab = $_SESSION['genSuivante'];
}
/*function affichTab($taille, $tab){
    for($i=0; $i < $taille; $i++){
        for($j=0; $j < $taille; $j++){
            echo $tab[$i][$j];
        }   
        echo '<br>';
    }
}*/
function affichTab($taille, $tab){
    echo '<table>';
    for($i=0; $i < $taille; $i++){
        echo '<tr>';
        for($j=0; $j < $taille; $j++){
            echo '<td>' . $tab[$i][$j] . '</td>';
        }   
        echo '</tr>';
    }
    echo '</table>';
}
affichTab($taille, $tab);
function voisin($tab,$i,$j){
    $nbrvoisins = 0;
    if(isset($tab[$i-1][$j]) && $tab[$i-1][$j] === 'O'){
        $nbrvoisins++;
    }
    if(isset($tab[$i-1][$j-1]) && $tab[$i-1][$j-1] === 'O'){
        $nbrvoisins++;
    }
    if(isset($tab[$i-1][$j+1]) && $tab[$i-1][$j+1] ==='O'){
        $nbrvoisins++;
    }
    if(isset($tab[$i][$j-1]) && $tab[$i][$j-1] === 'O'){
        $nbrvoisins++;
    }
    if(isset($tab[$i][$j+1]) && $tab[$i][$j+1] === 'O'){
        $nbrvoisins++;
    }
    if(isset($tab[$i+1][$j+1]) && $tab[$i+1][$j+1] === 'O'){
        $nbrvoisins++;
    }
    if(isset($tab[$i+1][$j-1]) && $tab[$i+1][$j-1] === 'O'){
        $nbrvoisins++;
    }
    if(isset($tab[$i+1][$j]) && $tab[$i+1][$j] === 'O'){
        $nbrvoisins++;
    }
    return $nbrvoisins;
}

function generationSuivante($tab, $taille, $tab2){
    for($i=0; $i < $taille; $i++){
        for($j=0; $j < $taille; $j++){
            if($tab[$i][$j] === 'O'){
                if(voisin($tab, $i, $j) == 2 || voisin($tab, $i, $j) == 3){
                    $tab2[$i][$j] = 'O';
                }
                else{
                    $tab2[$i][$j] = ' ';
                }
            }
            else{
              if(voisin($tab, $i, $j) == 3){
                    $tab2[$i][$j] = 'O';
                }
                else{
                    $tab2[$i][$j] = ' ';
                }  
            }
        }
    }
    return $tab2;
}

$_SESSION['genSuivante'] = generationSuivante($tab, $taille, $tab2);
?>